# Changelog

All notable changes to this project will be documented in this file.

## [2.3.1](https://gitlab.com/biomedit/django-identities/-/releases/2%2E3%2E1) - 2023-07-24

[See all changes since the last release](https://gitlab.com/biomedit/django-identities/compare/2%2E3%2E0...2%2E3%2E1)


### Bug Fixes

- **release-pypi:** Can't have direct dependency `biomedit-lints` ([490fa95](https://gitlab.com/biomedit/django-identities/commit/490fa95acebc76e38ae75d63973963df3fa839f9))
# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [2.3.0](https://gitlab.com/biomedit/django-identities/compare/2.2.3...2.3.0) (2023-07-24)


### Features

* **oidc:** do not overspecialize claims names ([d970c5e](https://gitlab.com/biomedit/django-identities/commit/d970c5e5d2c39058eb88869c41ec25656f1d308f))

### [2.2.3](https://gitlab.com/biomedit/django-identities/compare/2.2.2...2.2.3) (2022-11-08)

### [2.2.2](https://gitlab.com/biomedit/django-identities/compare/2.2.1...2.2.2) (2022-11-04)

### [2.2.1](https://gitlab.com/biomedit/django-identities/compare/2.2.0...2.2.1) (2022-11-04)


### Bug Fixes

* **logout:** remove OIDC RP-Initiated Logout incompatible implementation ([59c8e5d](https://gitlab.com/biomedit/django-identities/commit/59c8e5d0490074a4d5689bf774e5a7bc24a10631))

## [2.2.0](https://gitlab.com/biomedit/django-identities/compare/2.1.1...2.2.0) (2021-11-17)


### Features

* **profile:** add `affiliation_home` to profile serializer ([6bae63c](https://gitlab.com/biomedit/django-identities/commit/6bae63c4051123c69db474c56d95ce385be768fe))

### [2.1.1](https://gitlab.com/biomedit/django-identities/compare/2.1.0...2.1.1) (2021-08-02)

### Bug Fixes

- add a missing migration ([1c3fa1e](https://gitlab.com/biomedit/django-identities/commit/1c3fa1e94296574df300e869fec8afd6d02583c3))

## [2.1.0](https://gitlab.com/biomedit/django-identities/compare/2.0.0...2.1.0) (2021-07-30)

### Features

- **models:** add history for changes on `User` ([d520ae1](https://gitlab.com/biomedit/django-identities/commit/d520ae10184f04b73d6af94a264fdbf2709880d0)), closes [portal#422](https://git.dcc.sib.swiss/biwg/portal/issues/422)

## [2.0.0](https://gitlab.com/biomedit/django-identities/compare/1.1.0...2.0.0) (2021-07-23)

### ⚠ BREAKING CHANGES

- **model:** 'IsStaffOrTechAccount' and associated helper method 'is_tech_account' have been removed.

Use 'IsStaff' resp. 'is_staff' instead. And replace the 'IsTechAccount' part with appropriate finer granular permissions.

### Features

- **model:** remove 'is_tech_account' field from 'Profile' model ([c193432](https://gitlab.com/biomedit/django-identities/commit/c1934326aa02d72158cb776988b8cb35dc51641e)), closes [#2](https://gitlab.com/biomedit/django-identities/issues/2)

## [1.1.0](https://gitlab.com/biomedit/django-identities/compare/1.0.4...1.1.0) (2021-07-05)

### Features

- expose factories ([2cf5503](https://gitlab.com/biomedit/django-identities/commit/2cf55033eb6604e8be67d47c1757ea127f93c0d0))

### [1.0.4](https://gitlab.com/biomedit/django-identities/compare/1.0.3...1.0.4) (2021-06-23)

### [1.0.3](https://gitlab.com/biomedit/django-identities/compare/1.0.2...1.0.3) (2021-06-23)

### [1.0.2](https://gitlab.com/biomedit/django-identities/compare/1.0.1...1.0.2) (2021-06-07)

### [1.0.1](https://gitlab.com/biomedit/django-identities/compare/1.0.0...1.0.1) (2021-06-04)

### Bug Fixes

- fix pylint issues ([a016b3c](https://gitlab.com/biomedit/django-identities/commit/a016b3ceeb405a8ac8a8aded633df069d0c371f4))

## 1.0.0 (2021-06-03)
