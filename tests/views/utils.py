from django.contrib.auth import get_user_model
from django.urls import reverse
from rest_framework import status

from identities.tests.factories import USER_PASSWORD

User = get_user_model()


def assert_access_forbidden(
    test_class,
    url,
    users=(),
    test_unauthorized=True,
    methods=("get",),
    return_code=status.HTTP_403_FORBIDDEN,
):
    for method in methods:
        with test_class.subTest(method=method):
            # Unauthenticated is not allowed
            if test_unauthorized:
                test_class.assertEqual(
                    getattr(test_class.client, method)(url).status_code, return_code
                )
            for username, user_return_code in users:
                with test_class.subTest(user=username):
                    test_class.client.login(username=username, password=USER_PASSWORD)
                    test_class.assertEqual(
                        getattr(test_class.client, method)(url).status_code,
                        user_return_code,
                    )
                    test_class.client.logout()


def check_method(
    test_class,
    method,
    url,
    allowed,
    forbidden,
    data=None,
    ids=(),
    test_unauthorized=True,
    lookup_field="pk",
    **kwargs,
):
    m = getattr(test_class.client, method)
    # Unauthenticated is not allowed
    id_kwargs = ({lookup_field: i} for i in ids)
    args = () if data is None else (data,)
    if not ids:
        id_kwargs = ({},)
    status_code = {
        "post": status.HTTP_201_CREATED,
        "delete": status.HTTP_204_NO_CONTENT,
        "patch": status.HTTP_200_OK,
    }[method]
    url_suffix = {"post": "-list", "delete": "-detail", "patch": "-detail"}[method]
    for i_kwargs in id_kwargs:
        url = reverse(url + url_suffix, kwargs=i_kwargs)
        if test_unauthorized:
            test_class.assertEqual(m(url).status_code, status.HTTP_403_FORBIDDEN)
        for username in forbidden:
            test_class.client.login(username=username, password=USER_PASSWORD)
            test_class.assertEqual(
                status.HTTP_403_FORBIDDEN,
                m(url, *args, **kwargs).status_code,
                f"Wrong HTTP status for username '{username}'",
            )
            test_class.client.logout()
        for username in allowed:
            test_class.client.login(username=username, password=USER_PASSWORD)
            r = m(url, *args, **kwargs)
            test_class.assertEqual(r.status_code, status_code)
            test_class.client.logout()
