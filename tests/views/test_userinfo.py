from django.urls import reverse
from rest_framework import status


URL = reverse("userinfo")


def test_get_unauthenticated(client):
    assert client.get(URL).status_code == status.HTTP_403_FORBIDDEN


def test_get(client, basic_user_auth):
    response = client.get(URL)
    data = response.json()
    display_name = (
        f"{basic_user_auth.first_name} {basic_user_auth.last_name} "
        f"({basic_user_auth.email})"
    )
    data_ref = {
        "username": basic_user_auth.username,
        "email": basic_user_auth.email,
        "first_name": basic_user_auth.first_name,
        "last_name": basic_user_auth.last_name,
        "permissions": {
            "staff": False,
            "group_manager": False,
        },
        "profile": {
            "affiliation": basic_user_auth.profile.affiliation,
            "affiliation_id": basic_user_auth.profile.affiliation_id,
            "affiliation_home": "bar.org,foo.edu",
            "display_name": display_name,
            "display_id": None,
        },
    }
    assert response.status_code == status.HTTP_200_OK
    data_id = data.pop("id")
    ip_address = data.pop("ip_address")
    assert data == data_ref
    assert isinstance(data_id, int)
    assert isinstance(ip_address, str)
