import pytest
from django.contrib.auth import get_user_model
from django.urls import reverse
from rest_framework import status

from identities.tests.factories import USER_PASSWORD

User = get_user_model()
APPLICATION_JSON = "application/json"


class TestUserView:
    @pytest.fixture(autouse=True)
    def before_each(self, user_factory):
        # pylint: disable=attribute-defined-outside-init
        self.staff = user_factory(staff=True)
        self.basic_user = user_factory(basic=True)
        # pylint: enable=attribute-defined-outside-init

    def test_view_list(self, client):
        url = reverse("user-list")
        # Basic user
        client.login(username=self.basic_user.username, password=USER_PASSWORD)
        r = client.get(url)
        assert r.status_code == status.HTTP_200_OK
        assert len(r.json()) == 1
        assert r.json()[0]["username"] == self.basic_user.username
        client.logout()
        # Powerusers
        for username in (self.staff.username,):
            client.login(username=username, password=USER_PASSWORD)
            # All
            r = client.get(url)
            assert r.status_code == status.HTTP_200_OK
            assert len(r.json()) == User.objects.count()
            # Filter by username / email
            r = client.get(url, {"search": self.basic_user.username})
            assert r.status_code == status.HTTP_200_OK
            assert len(r.json()) == 1
            client.logout()

    def test_view_list_hide_not_active_users(self, client):
        url = reverse("user-list")
        user_count = User.objects.count()
        client.login(username=self.staff.username, password=USER_PASSWORD)

        assert len(client.get(url).json()) == user_count

        # inactive users are hidden by default
        self.basic_user.is_active = False
        self.basic_user.save()
        assert len(client.get(url).json()) == user_count - 1

        # adding request parameter includes inactive users as well
        assert len(client.get(url, {"include_inactive": True}).json()) == user_count

        client.logout()

    def test_update_as_staff(self, client):
        url = reverse("user-detail", kwargs={"pk": self.basic_user.pk})
        client.login(username=self.staff.username, password=USER_PASSWORD)

        # should be able to change `is_active` to `True` again"):
        r = client.patch(
            url,
            {"is_active": True},
            content_type=APPLICATION_JSON,
        )
        assert r.status_code == status.HTTP_200_OK

        client.logout()

    def test_create_local_user(self, client):
        url = reverse("user-list")
        data = {
            "first_name": "Chuck",
            "last_name": "Norris",
            "email": "local-chuck@norris.gov",
            "profile": {},
        }

        # Creating local user as non-staff user is forbidden
        client.login(username=self.basic_user.username, password=USER_PASSWORD)
        r = client.post(url, data, content_type=APPLICATION_JSON)
        assert r.status_code == status.HTTP_403_FORBIDDEN
        client.logout()

        # Create local user as staff
        client.login(username=self.staff.username, password=USER_PASSWORD)
        r = client.post(url, data, content_type=APPLICATION_JSON)
        response = r.json()
        assert r.status_code == status.HTTP_201_CREATED
        assert response["first_name"] == data["first_name"]
        assert response["last_name"] == data["last_name"]
        assert response["email"] == data["email"]
        assert response["username"] == data["email"]
        created_local_user = User.objects.get(id=response["id"])
        # make sure user cannot be used to log in
        assert created_local_user.has_usable_password() is False
        client.logout()

        # Cannot create user with non-unique email address
        client.login(username=self.staff.username, password=USER_PASSWORD)
        r = client.post(url, data, content_type=APPLICATION_JSON)
        assert r.status_code == status.HTTP_400_BAD_REQUEST
        assert r.json() == {"email": ["user with this email address already exists."]}
        client.logout()
