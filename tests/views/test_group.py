import logging

import pytest
from django.contrib.auth.models import Group, Permission
from django.contrib.contenttypes.models import ContentType
from django.urls import reverse
from guardian.models import GroupObjectPermission
from rest_framework import status

from identities.serializers.group import VALIDATION_ERROR_NON_STAFF_FIELDS
from identities.tests.factories import UserFactory, GroupFactory, USER_PASSWORD

VIEW_NAME = "group"
URL_LIST = reverse(f"{VIEW_NAME}-list")
GROUP_NAME = "New Group"


def test_get_groups_anonymous(client):
    assert client.get(URL_LIST).status_code == status.HTTP_403_FORBIDDEN


@pytest.mark.usefixtures("basic_user_auth")
def test_get_groups_authenticated(client):
    assert client.get(URL_LIST).status_code == status.HTTP_403_FORBIDDEN


def test_get_groups_permissions(admin_client):
    response = admin_client.get(URL_LIST)
    assert response.status_code == status.HTTP_200_OK
    assert len(response.json()) == 0

    GroupFactory.create()
    response = admin_client.get(URL_LIST)
    assert response.status_code == status.HTTP_200_OK
    assert len(response.json()) == 1


def test_get_groups_by_permission(admin_client, client, create_group_manager):
    managed_group_1, _ = GroupFactory.create_batch(2)

    # staff can see all groups
    response = admin_client.get(URL_LIST)
    assert response.status_code == status.HTTP_200_OK
    assert len(response.json()) == 2

    # group manager can only see their managed groups
    group_manager = create_group_manager(managed_group_1)
    client.login(username=group_manager.username, password=USER_PASSWORD)
    response = client.get(URL_LIST)
    assert response.status_code == status.HTTP_200_OK
    resp_json = response.json()
    assert len(resp_json) == 1
    assert resp_json[0]["name"] == managed_group_1.name
    client.logout()


def test_create_group(admin_client, user_factory, group_factory, caplog):
    caplog.set_level(logging.INFO)
    user1, user2 = user_factory.create_batch(2)
    group1, group2 = group_factory.create_batch(2)
    perm_add_group = Permission.objects.get(codename="add_group")
    perm_change_group = Permission.objects.get(codename="change_group")
    perm_delete_group = Permission.objects.get(codename="delete_group")
    group_name = GROUP_NAME

    data = {
        "name": group_name,
        "users": [user1.id, user2.id],
        "permissions": [perm_add_group.id],
        "permissions_object": [
            {
                "permission": perm_change_group.id,
                "objects": [group1.id, group2.id],
            },
            {"permission": perm_delete_group.id, "objects": [group2.id]},
        ],
    }
    response = admin_client.post(URL_LIST, data, content_type="application/json")
    assert response.status_code == status.HTTP_201_CREATED
    assert len(caplog.record_tuples) == 1
    assert caplog.record_tuples[0][1] == logging.INFO
    assert caplog.record_tuples[0][2].startswith("admin created")
    assert all(
        ele in caplog.record_tuples[0][2]
        for ele in (
            "change_group",
            "delete_group",
            GROUP_NAME,
            str(group1),
            str(group2),
        )
    )
    group = Group.objects.get(name=group_name)
    object_permissions = GroupObjectPermission.objects.all()
    assert len(object_permissions) == sum(
        len(x["objects"]) for x in data["permissions_object"]
    )
    assert set(
        (x.group, x.permission, x.content_object) for x in object_permissions
    ) == {
        (group, perm, proj)
        for perm, proj in (
            (perm_change_group, group1),
            (perm_change_group, group2),
            (perm_delete_group, group2),
        )
    }


def test_create_group_target_does_not_exist(admin_client, group_factory):
    group = group_factory()
    assert Group.objects.count() == 1
    perm_change_group = Permission.objects.get(codename="change_group")
    non_existing_pk = 1_000_000
    # Non-existing object
    response = admin_client.post(
        URL_LIST,
        {
            "name": GROUP_NAME,
            "users": [],
            "permissions_object": [
                {
                    "permission": perm_change_group.id,
                    "objects": [non_existing_pk],
                },
            ],
        },
        content_type="application/json",
    )
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json()["permissions_object"] == [
        {
            "non_field_errors": [
                f"Object {non_existing_pk} does not exist for model "
                f"{group._meta.model.__name__}"
            ]
        }
    ]
    # Non-existing permission
    response = admin_client.post(
        URL_LIST,
        {
            "name": GROUP_NAME,
            "users": [],
            "permissions_object": [
                {
                    "permission": non_existing_pk,
                    "objects": [group.id],
                },
            ],
        },
        content_type="application/json",
    )
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json()["permissions_object"] == [
        {"permission": [f"{Permission.__name__} {non_existing_pk} does not exist"]}
    ]
    assert Group.objects.count() == 1


def test_update_group(admin_client, group_factory, caplog):
    caplog.set_level(logging.INFO)
    group1, group2, group3 = group_factory.create_batch(3)
    perm_change_group = Permission.objects.get(codename="change_group")
    perm_delete_group = Permission.objects.get(codename="delete_group")
    group = GroupFactory.create()
    GroupObjectPermission.objects.bulk_create(
        GroupObjectPermission(
            group=group,
            content_type=ContentType.objects.get_for_model(Group),
            object_pk=str(proj.pk),
            permission=perm,
        )
        for perm, proj in (
            (perm_change_group, group1),
            (perm_change_group, group2),
            (perm_delete_group, group2),
        )
    )

    url = reverse(f"{VIEW_NAME}-detail", kwargs={"pk": group.pk})
    data = {
        "name": group.name,
        "users": [],
        "permissions": [],
        "permissions_object": [
            {
                "permission": perm_change_group.id,
                "objects": [group3.id, group2.id],
            },
        ],
    }
    response = admin_client.put(url, data, content_type="application/json")
    assert response.status_code == status.HTTP_200_OK
    # one log message for created, another for deleted
    assert len(caplog.record_tuples) == 2
    assert caplog.record_tuples[0][1] == logging.INFO
    object_permissions = GroupObjectPermission.objects.all()
    assert len(object_permissions) == sum(
        len(x["objects"]) for x in data["permissions_object"]
    )
    assert set(
        (x.group, x.permission, x.content_object) for x in object_permissions
    ) == {
        (group, perm, proj)
        for perm, proj in (
            (perm_change_group, group2),
            (perm_change_group, group3),
        )
    }

    # Add new permission without deleting anything
    caplog.clear()
    response = admin_client.patch(
        url,
        {
            "permissions_object": [
                {
                    "permission": perm_change_group.id,
                    "objects": [group3.id, group2.id],
                },
                {
                    "permission": perm_delete_group.id,
                    "objects": [group1.id],
                },
            ]
        },
        content_type="application/json",
    )
    assert len(caplog.record_tuples) == 1
    assert caplog.record_tuples[0][1] == logging.INFO
    assert GroupObjectPermission.objects.count() == 3


def test_delete_group(admin_client, group_factory):
    group = GroupFactory.create()
    permission_group_pk = group_factory().pk
    GroupObjectPermission.objects.create(
        group=group,
        content_type=ContentType.objects.get_for_model(Group),
        object_pk=str(permission_group_pk),
        permission=Permission.objects.get(codename="change_group"),
    )
    assert Group.objects.count() == 2
    assert GroupObjectPermission.objects.count() == 1
    response = admin_client.delete(
        reverse(f"{VIEW_NAME}-detail", kwargs={"pk": group.pk})
    )
    assert response.status_code == status.HTTP_204_NO_CONTENT
    assert Group.objects.count() == 1
    assert Group.objects.all()[0].pk == permission_group_pk
    assert GroupObjectPermission.objects.count() == 0


@pytest.mark.django_db
class TestGroupManagerUpdate:
    @staticmethod
    def get_url(group: Group):
        return reverse(f"{VIEW_NAME}-detail", kwargs={"pk": group.pk})

    @pytest.fixture(autouse=True)
    def setup(self, client, create_group_manager):
        # pylint: disable=attribute-defined-outside-init
        self.managed_group, self.unmanaged_group = GroupFactory.create_batch(2)
        self.group_manager = create_group_manager(self.managed_group)
        self.user = UserFactory.create()
        self.perm_group_admin = Permission.objects.get(codename="add_group")
        self.perm_change_group = Permission.objects.get(codename="change_group")
        GroupObjectPermission.objects.bulk_create(
            GroupObjectPermission(
                group=group,
                content_type=ContentType.objects.get_for_model(Group),
                object_pk=str(group.pk),
                permission=self.perm_group_admin,
            )
            for group, group in (
                (self.managed_group, self.managed_group),
                (self.unmanaged_group, self.unmanaged_group),
            )
        )
        self.data = {
            "name": self.managed_group.name,
            "users": [self.user.id],
            "permissions": [],
            "permissions_object": [
                {
                    "permission": self.perm_group_admin.id,
                    "objects": [self.managed_group.id],
                },
            ],
        }
        self.url = self.get_url(self.managed_group)
        self.client = client
        # pylint: enable=attribute-defined-outside-init

        self.client.login(username=self.group_manager.username, password=USER_PASSWORD)
        yield
        # teardown
        self.client.logout()

    def assert_error_response(self):
        error_response = self.client.put(
            self.url, self.data, content_type="application/json"
        )
        assert error_response.status_code == status.HTTP_400_BAD_REQUEST
        assert error_response.json() == [VALIDATION_ERROR_NON_STAFF_FIELDS]

    def test_can_update_users(self):
        # group manager can update `users` on groups they manage
        response = self.client.put(self.url, self.data, content_type="application/json")
        assert response.status_code == status.HTTP_200_OK
        assert response.json()["users"] == [self.user.id]

    def test_cannot_update_name(self):
        # group manager CANNOT update `name`
        self.data["name"] = "different"
        self.assert_error_response()

    def test_cannot_update_permissions(self):
        # group manager CANNOT update `permissions`
        self.data["permissions"] = [self.perm_change_group.id]
        self.assert_error_response()

    def test_cannot_update_permission_in_permissions_object(self):
        # group manager CANNOT update `permission` on an existing object in `permissions_object`
        self.data["permissions_object"][0]["permission"] = self.perm_change_group.id
        self.assert_error_response()

    def test_cannot_update_objects_in_permissions_object(self):
        # group manager CANNOT update `objects` on an existing object in `permissions_object`
        self.data["permissions_object"][0]["objects"] = [self.unmanaged_group.id]
        self.assert_error_response()

    def test_cannot_add_object_to_objects_in_permissions_object(self):
        # group manager CANNOT add an object to `objects` on existing object in `permissions_object`
        self.data["permissions_object"][0]["objects"].append(self.unmanaged_group.id)
        self.assert_error_response()

    def test_cannot_add_new_object_to_permissions_object(self):
        # group manager CANNOT add a new object to `permissions_object`
        self.data["permissions_object"].append(
            {
                "permission": self.perm_group_admin.id,
                "objects": [self.unmanaged_group.id],
            }
        )
        self.assert_error_response()

    def test_cannot_update_unmanaged_group_users(self):
        # group manager CANNOT update `users` on groups they DO NOT manage
        url = self.get_url(self.unmanaged_group)
        self.data["name"] = self.unmanaged_group.name
        response = self.client.put(url, self.data, content_type="application/json")
        assert response.status_code == status.HTTP_404_NOT_FOUND
