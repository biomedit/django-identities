from django.contrib.auth.models import Group

from identities.serializers import AnyObjectSerializer


def test_generic_object_serializer():
    g = Group(name="New Group")
    assert AnyObjectSerializer(g).data == {"id": g.id, "name": str(g)}
