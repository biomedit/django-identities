import pytest

from django.contrib.auth.models import Group, AbstractUser
from guardian.shortcuts import assign_perm

from identities.permissions import perm_group_manager

from identities.tests.factories import UserFactory, USER_PASSWORD, GroupFactory


def make_group_manager(user: AbstractUser, managed_group: Group):
    group, _ = Group.objects.get_or_create(name=f"Group Manager {managed_group.name}")
    group.user_set.add(user)  # type: ignore
    assign_perm(perm_group_manager, group, managed_group)


@pytest.fixture
def user_factory(db):  # pylint: disable=unused-argument
    return UserFactory


@pytest.fixture
def group_factory(db):  # pylint: disable=unused-argument
    return GroupFactory


@pytest.fixture
def create_group_manager():
    def _method(group: Group):
        user = UserFactory.create()
        make_group_manager(user, group)
        return user

    return _method


@pytest.fixture
def basic_user_auth(db, client):  # pylint: disable=unused-argument
    """Return authenticated basic user"""
    user = UserFactory(basic=True)
    client.login(username=user.username, password=USER_PASSWORD)
    yield user
    client.logout()


@pytest.fixture
def staff_user_auth(db, client):  # pylint: disable=unused-argument
    """Return authenticated staff user"""
    user = UserFactory(staff=True)
    client.login(username=user.username, password=USER_PASSWORD)
    yield user
    client.logout()
