import os
from dataclasses import dataclass

from django_drf_utils.config import BaseConfig, Oidc

# SECRET_KEY and django.contrib.admin (in INSTALLED_APPS) are only necessary
# in one test for Permission.objects.get(codename="change_logentry")
SECRET_KEY = "testkey"

INSTALLED_APPS = [
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.messages",
    "django.contrib.sessions",
    "rest_framework",
    "guardian",
    "identities",
]

MIDDLEWARE = [
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
]

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
        },
    }
]

DEFAULT_AUTO_FIELD = "django.db.models.BigAutoField"

ROOT_URLCONF = "identities.urls"

BASE_DIR = os.path.dirname(os.path.abspath(__file__))

DATABASES = {
    "default": {
        "CONN_MAX_AGE": 0,
        "ENGINE": "django.db.backends.sqlite3",
        "HOST": "localhost",
        "NAME": os.path.join(BASE_DIR, "db.sqlite3"),
        "PASSWORD": "",
        "PORT": "",
        "USER": "",
    }
}

AUTH_USER_MODEL = "identities.User"
GUARDIAN_MONKEY_PATCH = False
GUARDIAN_GET_INIT_ANONYMOUS_USER = "identities.models.get_anonymous_user_instance"

REST_FRAMEWORK = {
    "DEFAULT_AUTHENTICATION_CLASSES": (
        "rest_framework.authentication.SessionAuthentication",
        "rest_framework.authentication.BasicAuthentication",
    ),
    "DEFAULT_PERMISSION_CLASSES": ("rest_framework.permissions.IsAuthenticated",),
}

AUTHENTICATION_BACKENDS = [
    "django.contrib.auth.backends.ModelBackend",
    "guardian.backends.ObjectPermissionBackend",
]


@dataclass
class ConfigTest(BaseConfig):
    name: str
    oidc: Oidc


CONFIG = ConfigTest.from_dict(
    data={
        "name": "no name",
        "oidc": {
            "client_id": "test",
            "client_secret": "client_secret",
            "config_url": "https://keycloak.example/auth/realms/test/.well-known/openid-configuration",
            "mapper": {
                "first_name": "given_name",
                "last_name": "family_name",
                "email": "email",
                "affiliation_id": "linkedAffiliationUniqueID",
                "affiliation": "linkedAffiliation",
            },
        },
    }
)
