# pylint: disable=unused-import
from identities.views.group import GroupViewSet
from identities.views.permission import PermissionViewSet, ObjectByPermissionViewSet
from identities.views.user import UserViewSet, UserinfoView
